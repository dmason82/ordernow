package angelhack.summer12;

import java.util.ArrayList;
import java.util.List;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MenuSection extends SherlockActivity implements OnItemClickListener {
	
	OrderNow order;
	FoodItemAdapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menusection_layout);
		order = (OrderNow)getApplicationContext();
		ListView list = (ListView)findViewById(R.id.listViewMenuSection);
		TextView section = (TextView)findViewById(R.id.textView1);
		adapter = new FoodItemAdapter(this,R.layout.menu_list_item,new ArrayList<FoodItem>());
		Log.v("Intent",getIntent().toString());
		if(getIntent().hasExtra("list")){
			if(getIntent().getExtras().getString("list").equals("beverages")){
				adapter = new FoodItemAdapter(this,R.layout.menu_layout,order.drinks);
				Log.v("drinks selected",order.drinks.get(0).name.toString());
				section.setText("Drinks");
			}
			if(getIntent().getExtras().getString("list").equals("pastries"))
			{
				adapter = new FoodItemAdapter(this,R.layout.menu_layout,order.pastries);
				section.setText("Pastries");
				
			}
			if(getIntent().getExtras().getString("list").equals("sandwiches")){
				adapter = new FoodItemAdapter(this,R.layout.menu_layout,order.sandwiches);
				section.setText("Sandwiches");
			}
			if(getIntent().getExtras().getString("list").equals("more")){
				adapter = new FoodItemAdapter(this,R.layout.menu_layout,order.more);
				section.setText("More");
			}
			
		}
		list.setAdapter(adapter);
		list.setOnItemClickListener(this);
	}
	
	public ArrayList<FoodItem> MenuSection;
	
	private class FoodItemAdapter extends ArrayAdapter<FoodItem> {
		 
        private ArrayList<FoodItem> items;
        
        
        public FoodItemAdapter(Context context, int textViewResourceId, ArrayList<FoodItem> items) {
                 super(context, textViewResourceId, items);
                 this.items = items;
         }
 
        @Override
         public View getView(int position, View convertView, ViewGroup parent) {
                 View v = convertView;
                 if (v == null) {
                     LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                     v = vi.inflate(R.layout.menu_list_item, null);
                 }
                 FoodItem f = this.items.get(position);
                 if (f != null) {
                	
                         TextView nameText = (TextView) v.findViewById(R.id.nameView);
                         TextView priceText = (TextView) v.findViewById(R.id.priceView);
                         ImageView image = (ImageView) v.findViewById(R.id.imageView1);
                         if (nameText != null) {
                               nameText.setText(f.getname());   
                         }

                         if(priceText != null){
                             priceText.setText(f.getprice());
                       }
                         if(image !=null)
                         {
                        	 image.setImageDrawable(getResources().getDrawable(f.getpic()));
                         }
                 }
                 return v;
         }
 }
	
    public class ItemAdapter extends ArrayAdapter<FoodItem>
    {
    	private Context ctx;
    	List<FoodItem> items;
    	public ItemAdapter(Context context,int layoutResourceId, List<FoodItem> data)
    	{
    		super(context,layoutResourceId,data);
    		this.ctx = context;
    		this.items = data;
    	}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (view == null)
			{
				LayoutInflater li = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = li.inflate(R.layout.menu_layout, null);
			}
			ImageView picture = (ImageView)view.findViewById(R.id.imageView1);	
			TextView nameText = (TextView)view.findViewById(R.id.nameView);
			TextView descText = (TextView)view.findViewById(R.id.descriptionText);
			TextView priceText = (TextView)view.findViewById(R.id.priceView);
			FoodItem fi = items.get(position);
			picture.setImageDrawable(getResources().getDrawable(fi.picture));
			nameText.setText(fi.name);
			descText.setText(fi.description);
			priceText.setText(fi.price.toString());
			return view;
		}
		
    }

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		// TODO Auto-generated method stub
//		Intent i = new Intent();
//		i.setClass(getBaseContext(), ShoppingCart.class);
//		startActivity(i);	
		order.shoppingCart.add(adapter.getItem(position));
		Log.v("Number of things in cart ",Integer.toString(order.shoppingCart.size()));
	}

	
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.order, menu);
		return true;
    }
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		Intent i = new Intent();
		switch(item.getItemId())
		{
		case R.id.shoppingList:
			i.setClass(getBaseContext(), ShoppingCart.class);
			startActivity(i);
		}
		return true;
	}

}
