package angelhack.summer12;

import java.util.ArrayList;

import com.actionbarsherlock.app.SherlockActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Button;

public class ShoppingCart extends SherlockActivity implements OnClickListener {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shopping_cart);
		ListView list = (ListView)findViewById(R.id.shoppingList);
		OrderNow order = (OrderNow)getApplicationContext();
		list.setAdapter(new FoodItemAdapter(this,R.layout.menu_layout,order.shoppingCart));
		Button orderButton = (Button)findViewById(R.id.button1);
		float total=0.0f,tax=0.0f;
		for(FoodItem fi: order.shoppingCart )
		{
			total+=fi.price.floatValue();
			tax +=fi.price.floatValue() *.09;
		}
		TextView totalTxt = (TextView)findViewById(R.id.total);
		totalTxt.setText(String.format("%.02f", total));
		
		TextView taxTxt = (TextView)findViewById(R.id.tax);
		taxTxt.setText(String.format("%.02f", tax));
		orderButton.setOnClickListener(this);
	}

	public ArrayList<FoodItem> ShoppingCart;
	
	private class FoodItemAdapter extends ArrayAdapter<FoodItem> {
		 
        private ArrayList<FoodItem> items;
        
        
        public FoodItemAdapter(Context context, int textViewResourceId, ArrayList<FoodItem> items) {
                 super(context, textViewResourceId, items);
                 this.items = items;
         }
 
        @Override
         public View getView(int position, View convertView, ViewGroup parent) {
                 View v = convertView;
                 if (v == null) {
                     LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                     v = vi.inflate(R.layout.menu_layout, null);
                 }
                 FoodItem f = items.get(position);
                 if (f != null) {
                	
                         TextView nameText = (TextView) v.findViewById(R.id.nameView);
                         TextView descriptionText = (TextView) v.findViewById(R.id.descriptionText);
                         TextView priceText = (TextView) v.findViewById(R.id.priceView);
                         ImageView image = (ImageView) v.findViewById(R.id.imageView1);
                         if (nameText != null) {
                               nameText.setText(f.getname());   
                         }
                         if(descriptionText != null){
                        	 descriptionText.setText(f.getdescription());
                         }
                         if(priceText != null){
                             priceText.setText(f.getprice());
                       }
                         if(image !=null)
                         {
                        	 image.setImageDrawable(getResources().getDrawable(f.getpic()));
                         }
                 }
                 return v;
         }
 }

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch(arg0.getId()){
		case R.id.button1:
			Intent i = new Intent();
			i.setClass(getApplicationContext(), PayCode.class);
			startActivity(i);
		}
	}

}

