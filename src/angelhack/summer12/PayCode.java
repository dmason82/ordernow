package angelhack.summer12;

import com.actionbarsherlock.app.SherlockActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.view.View;
import android.widget.EditText;
import android.view.View.OnClickListener;

public class PayCode extends SherlockActivity implements OnClickListener {
	private EditText password;
	private float total;
	private OrderNow app;
	private String items;
	private TextView totalText,itemsText;
	@Override
	protected void onCreate(Bundle savedInstanceState)   {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pay_code);
		items = "";
		app = (OrderNow)getApplicationContext();
		total = 0.0f;
		password = (EditText)findViewById(R.id.editText1);
		password.setText("");
		totalText = (TextView)findViewById(R.id.textView2);
		itemsText = (TextView)findViewById(R.id.textView3);
		
		for(FoodItem fi : app.shoppingCart)
		{
			if(items.equals(""))
			{
				items+=fi.name;
				total+=fi.price.floatValue();
			}
			else
			{
				items+=", "+fi.name;
				total+=fi.price.floatValue();
			}
		}
		totalText.setText( String.format("%.02f", total));
		itemsText.setText(items);
		/**
		 * Buttons for wiring for our pay code.
		 */
		Button one = (Button)findViewById(R.id.button1);
		one.setOnClickListener(this);
		Button two = (Button)findViewById(R.id.button2);
		two.setOnClickListener(this);
		Button three=(Button)findViewById(R.id.button3);
		three.setOnClickListener(this);
		Button four = (Button)findViewById(R.id.button4);
		four.setOnClickListener(this);
		Button five = (Button)findViewById(R.id.button5);
		five.setOnClickListener(this);
		Button six = (Button)findViewById(R.id.button6);
		six.setOnClickListener(this);
		Button seven = (Button)findViewById(R.id.button7);
		seven.setOnClickListener(this);
		Button eight = (Button)findViewById(R.id.button8);
		eight.setOnClickListener(this);
		Button nine = (Button)findViewById(R.id.button9);
		nine.setOnClickListener(this);
		Button ten = (Button)findViewById(R.id.button10);
		ten.setOnClickListener(this);
		
	}
	
	
	
	@Override
	public void onClick(View v)
	{
		switch(v.getId())
		{
		case R.id.button1:
			password.setText(password.getText()+"1");
			break;
		case R.id.button2:
			password.setText(password.getText()+"2");
			break;
		case R.id.button3:
			password.setText(password.getText()+"3");
			break;
		case R.id.button4:
			password.setText(password.getText()+"4");
			break;
		case R.id.button5:
			password.setText(password.getText()+"5");
			break;
		case R.id.button6:
			password.setText(password.getText()+"6");
			break;
		case R.id.button7:
			password.setText(password.getText()+"7");
			break;
		case R.id.button8:
			password.setText(password.getText()+"8");
			break;
		case R.id.button9:
			password.setText(password.getText()+"9");
			break;
		case R.id.button10:
			Intent i = new Intent();
			i.setClass(getBaseContext(), OrderComplete.class);
			startActivity(i);
			break;
		default:
			break;
		}
		password.moveCursorToVisibleOffset();
//		Log.v("Password",password.getText());
	}
}
