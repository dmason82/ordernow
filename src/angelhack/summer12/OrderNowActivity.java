package angelhack.summer12;


import java.util.ArrayList;
import java.util.List;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.content.Intent;

public class OrderNowActivity extends SherlockActivity implements OnItemClickListener,OnClickListener {
    /** Called when the activity is first created. */
	 ArrayList<FoodItem> food ;
	Integer[] pictures = {R.drawable.restaurant_images,R.drawable.muffin,R.drawable.coffee};
	ItemAdapter foodAdapter;
	ArrayList<Integer> pics;
    @SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
    	DisplayMetrics metrics = getBaseContext().getResources().getDisplayMetrics();
    	float dp = 20f;
    	float fpixels = metrics.density * dp;
        super.onCreate(savedInstanceState);
        OrderNow order = (OrderNow)getApplicationContext();
        setContentView(R.layout.main);
        ListView list = (ListView)findViewById(R.id.listView1);
        ListView instant = (ListView)findViewById(R.id.instantList);
        this.food = order.food;
        this.foodAdapter = new ItemAdapter(this,R.layout.menu_layout,food);
        FoodItem instantOrder = order.instant.get(0); 
        ItemAdapter instantAdapter = new ItemAdapter(this,R.layout.menu_layout,order.instant);
        list.setAdapter(foodAdapter);
        
        list.setOnItemClickListener(this);
        if (order.food.isEmpty())
        {
        	list.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,0));
        }
        else
        {
        	list.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,(int) ((metrics.density*100f)+0.5f)));
        }
        if(instantOrder.price.floatValue() == 0.0f)
        {
        	instant.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,0));
        }
        else{
        	instant.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,(int) ((metrics.density*51f)+0.5f)));
        }
        instant.setAdapter(instantAdapter);
        instant.setOnItemClickListener(this);
        TextView drinks = (TextView)findViewById(R.id.textViewDrinks);
        drinks.setOnClickListener(this);
        TextView sandwiches = (TextView)findViewById(R.id.textViewSandwiches);
        sandwiches.setOnClickListener(this);
        TextView pastries = (TextView)findViewById(R.id.textViewPastries);
        pastries.setOnClickListener(this);
        TextView more = (TextView)findViewById(R.id.textViewMore);
        more.setOnClickListener(this);
    }
    
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.order, menu);
		return true;
    }

	public class ImageAdapter extends BaseAdapter {
    	int imageBackground;
    	Context ctx;
    	
    	public ImageAdapter(Context c) {
    		// TODO Auto-generated constructor stub
    		super();
    		ctx = c;
    		TypedArray array = obtainStyledAttributes(R.styleable.Gallery1);
    		array.getResourceId(R.styleable.Gallery1_android_galleryItemBackground, 0);
    	}

    	@Override
    	public int getCount() {
    		// TODO Auto-generated method stub
    		return 0;
//    		return pics.count ;
    	}

    	@Override
    	public Object getItem(int arg0) {
    		// TODO Auto-generated method stub
    		return null;
    	}

    	@Override
    	public long getItemId(int arg0) {
    		// TODO Auto-generated method stub
    		return 0;
    	}

    	@Override
    	public View getView(int arg0, View arg1, ViewGroup arg2) {
    		// TODO Auto-generated method stub
    		return null;
    	}

    }
    
	public boolean onOptionsItemSelected(MenuItem item)
	{
		Intent i = new Intent();
		switch(item.getItemId())
		{
		case R.id.shoppingList:
			i.setClass(getBaseContext(), ShoppingCart.class);
			startActivity(i);
		}
		return true;
	}
    public class ItemAdapter extends ArrayAdapter<FoodItem>
    {
    	private Context ctx;
    	List<FoodItem> items;
    	public ItemAdapter(Context context,int layoutResourceId, List<FoodItem> data)
    	{
    		super(context,layoutResourceId,data);
    		this.ctx = context;
    		this.items = data;
    	}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (view == null)
			{
				LayoutInflater li = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = li.inflate(R.layout.menu_layout, null);
			}
			ImageView picture = (ImageView)view.findViewById(R.id.imageView1);	
			TextView nameText = (TextView)view.findViewById(R.id.nameView);
			TextView descText = (TextView)view.findViewById(R.id.descriptionText);
			TextView priceText = (TextView)view.findViewById(R.id.priceView);
			FoodItem fi = items.get(position);
			picture.setImageDrawable(getResources().getDrawable(fi.picture));
			nameText.setText(fi.name);
			descText.setText(fi.description);
			priceText.setText(String.format("%.2f", fi.price));
			return view;
		}
		
    }
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		OrderNow ord = (OrderNow)getApplicationContext();
		switch(arg0.getId())
		{
		case R.id.listView1:

			ord.shoppingCart.add(this.food.get(arg2));
			Log.v("Number of things in cart ",Integer.toString(ord.shoppingCart.size()));
			break;
		case R.id.instantList:
			for(FoodItem fi : foodAdapter.items){
				ord.shoppingCart.add(fi);
			}
			Log.v("instant order selected",this.food.toString());
			break;
		}
		if(arg0.getId() == R.id.instantList)
		{
			Intent i = new Intent();
			i.setClass(getApplicationContext(), PayCode.class);
			startActivity(i);
		}
	}
	@Override
	public void onClick(View v)
	{
		Intent i = new Intent();
		i.setClass(getBaseContext(), MenuSection.class);
		switch( v.getId()){
		case R.id.textViewDrinks:
			i.putExtra("list","beverages");
			break;
		case R.id.textViewMore:
			i.putExtra("list","more");
			break;
		case R.id.textViewPastries:
			i.putExtra("list", "pastries");
			break;
		case R.id.textViewSandwiches:
			i.putExtra("list", "sandwiches");
			break;
			default:
				break;
		}
		startActivity(i);
	}

}
