package angelhack.summer12;

import java.util.ArrayList;
import java.util.List;

import com.actionbarsherlock.app.SherlockActivity;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class OrderComplete extends SherlockActivity implements OnClickListener,OnItemClickListener {
	OrderNow app;
	@Override
	public void onCreate(Bundle saveState)
	{
    	DisplayMetrics metrics = getBaseContext().getResources().getDisplayMetrics();
    	float dp = 20f;
    	float fpixels = metrics.density * dp;
		super.onCreate(saveState);
		app = (OrderNow)getApplicationContext();
		setContentView(R.layout.order_complete);
		Button b = (Button)findViewById(R.id.completeOrderButton);
		b.setOnClickListener(this);
		ListView lv = (ListView)findViewById(R.id.listView1);
		lv.setAdapter(new FoodItemAdapter(this,R.layout.menu_list_item,app.shoppingCart));
	      ListView instant = (ListView)findViewById(R.id.instantList);
	    FoodItem instantOrder = app.instant.get(0); 
	     ItemAdapter instantAdapter = new ItemAdapter(this,R.layout.menu_layout,app.instant);
	        if(instantOrder.price.floatValue() == 0.0f)
	        {
	    		for(FoodItem fi:app.shoppingCart)
	    		{
	    			if(app.food.isEmpty())
	    			{
	    				app.food.add(fi);
	    				instantOrder.description+=fi.name;
	    				instantOrder.price = new Float(instantOrder.price.floatValue()+fi.price);
	    			}
	    			else{
	    			app.food.add(fi);
	    			instantOrder.description+=" & "+fi.name;
	    			instantOrder.price = new Float(instantOrder.price.floatValue()+fi.price);
	    			}
	    		}
	        	
	        }
        instant.setAdapter(instantAdapter);
        instant.setOnItemClickListener(this);
	}
	
	
	
    public class ItemAdapter extends ArrayAdapter<FoodItem>
    {
    	private Context ctx;
    	List<FoodItem> items;
    	public ItemAdapter(Context context,int layoutResourceId, List<FoodItem> data)
    	{
    		super(context,layoutResourceId,data);
    		this.ctx = context;
    		this.items = data;
    	}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (view == null)
			{
				LayoutInflater li = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = li.inflate(R.layout.menu_layout, null);
			}
			ImageView picture = (ImageView)view.findViewById(R.id.imageView1);	
			TextView nameText = (TextView)view.findViewById(R.id.nameView);
			TextView descText = (TextView)view.findViewById(R.id.descriptionText);
			TextView priceText = (TextView)view.findViewById(R.id.priceView);
			FoodItem fi = items.get(position);
			picture.setImageDrawable(getResources().getDrawable(fi.picture));
			nameText.setText(fi.name);
			descText.setText(fi.description);
			priceText.setText(String.format("%.2f", fi.price));
			return view;
		}
		
    }
	
	@Override
	public void onClick(View v)
	{
		OrderNow app = (OrderNow)getApplicationContext();
		app.food.clear();
		FoodItem instant = app.instant.get(0);
		instant.price = new Float(0.0f);
		for(FoodItem fi:app.shoppingCart)
		{
			if(app.food.isEmpty())
			{
				app.food.add(fi);
//				instant.description+=fi.name;
				instant.price = new Float(instant.price.floatValue()+fi.price);
			}
			else{
			app.food.add(fi);
//			instant.description+=" & "+fi.name;
			instant.price = new Float(instant.price.floatValue()+fi.price);
			}
		}
		app.shoppingCart.clear();
		Intent i = new Intent();
		
		i.setClass(getBaseContext(),OrderNowActivity.class );
		startActivity(i);
	}
	
	/**
	 * 
	 *
	 */
	private class FoodItemAdapter extends ArrayAdapter<FoodItem> {
		 
        private ArrayList<FoodItem> items;
        
        
        public FoodItemAdapter(Context context, int textViewResourceId, ArrayList<FoodItem> items) {
                 super(context, textViewResourceId, items);
                 this.items = items;
         }
 
        @Override
         public View getView(int position, View convertView, ViewGroup parent) {
                 View v = convertView;
                 if (v == null) {
                     LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                     v = vi.inflate(R.layout.menu_list_item, null);
                 }
                 FoodItem f = items.get(position);
                 if (f != null) {
                	
                         TextView nameText = (TextView) v.findViewById(R.id.nameView);
                         TextView priceText = (TextView) v.findViewById(R.id.priceView);
                         ImageView image = (ImageView) v.findViewById(R.id.imageView1);
                         if (nameText != null) {
                               nameText.setText(f.getname());   
                         }
                         if(priceText != null){
                             priceText.setText(String.format("%.02f", f.price));
                       }
                         if(image !=null)
                         {
                        	 image.setImageDrawable(getResources().getDrawable(f.getpic()));
                         }
                 }
                 return v;
         }
 }

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		if(arg0.getId() == R.id.instantList)
		{
			Toast.makeText(this, "Current order saved to Instant Order", Toast.LENGTH_SHORT).show();
		}
		
	}

}
