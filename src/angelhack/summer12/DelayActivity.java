package angelhack.summer12;


import com.actionbarsherlock.app.SherlockActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

public class DelayActivity extends SherlockActivity {

	private class DelayAsyncTask extends AsyncTask<Void,Void,Void>
	{

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			Intent i = new Intent();
			i.setClass(getApplicationContext(), OrderNowActivity.class);
			startActivity(i);
		}
		
		
	}
	public void onCreate(Bundle save)
	{
		super.onCreate(save);
		setContentView(R.layout.delay);
		DelayAsyncTask task = new DelayAsyncTask();
		task.execute();
//		try {
//			Thread.sleep(5000);
//			Intent i = new Intent();
//			i.setClass(getApplicationContext(), OrderNowActivity.class);
//			startActivity(i);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
	}
}
