package angelhack.summer12;

import android.app.Application;
import java.util.ArrayList;
public class OrderNow extends Application {
public ArrayList<FoodItem> shoppingCart,food,drinks,pastries,sandwiches,more,instant;
@Override
public void onCreate() {
	// TODO Auto-generated method stub
	super.onCreate();
	shoppingCart = new ArrayList<FoodItem>();
	food = new ArrayList<FoodItem>();
	instant = new ArrayList<FoodItem>();
	drinks= new ArrayList<FoodItem>();
	pastries = new ArrayList<FoodItem>();
	sandwiches = new ArrayList<FoodItem>();
	more = new ArrayList<FoodItem>();
	
	this.drinks.add(new FoodItem(R.drawable.dcafeamericano,"Caffe Americano","",new Float(2.29)));
	this.drinks.add(new FoodItem(R.drawable.dcafelatte,"Caffe Latte","",new Float(3.29)));	
	this.drinks.add(new FoodItem(R.drawable.dcaffemocha,"Caffe Mocha","",new Float(3.59)));
	this.drinks.add(new FoodItem(R.drawable.dcappuccino,"Caffe Cappuccino","",new Float(3.29)));
	this.drinks.add(new FoodItem(R.drawable.ddripcoffee,"Brewed Coffee","",new Float(1.59)));
	this.drinks.add(new FoodItem(R.drawable.despresso,"Caffe Espresso","",new Float(2.99)));
	this.drinks.add(new FoodItem(R.drawable.dicedcoffee,"Iced Coffee","",new Float(2.29)));
	this.drinks.add(new FoodItem(R.drawable.dmacchiato,"Espresso Macchiato","",new Float(3.29)));
	this.drinks.add(new FoodItem(R.drawable.dhottea,"Hot Tea","",new Float(1.59)));
	this.drinks.add(new FoodItem(R.drawable.dicedtea,"Iced Tea","",new Float(1.59)));
	this.drinks.add(new FoodItem(R.drawable.dsmoothie,"Smoothie","",new Float(3.29)));
	
	this.pastries.add(new FoodItem(R.drawable.bbluemuffin,"Blueberry muffin","",new Float(1.99)));
	this.pastries.add(new FoodItem(R.drawable.pmuffin,"Bran muffin","",new Float(1.99)));
	this.pastries.add(new FoodItem(R.drawable.bbread,"Banana Bread","",new Float(1.99)));
	this.pastries.add(new FoodItem(R.drawable.bcroissant,"Croissant","",new Float(1.99)));
	this.pastries.add(new FoodItem(R.drawable.bcupcake,"Donut","",new Float(1.99)));
	this.pastries.add(new FoodItem(R.drawable.boatbar,"Oat Bar","",new Float(1.99)));
	this.pastries.add(new FoodItem(R.drawable.bscone,"Blueberry Scone","",new Float(1.99)));
	this.pastries.add(new FoodItem(R.drawable.pbrownie,"Brownie","",new Float(1.99)));
	this.pastries.add(new FoodItem(R.drawable.pcookie,"Chocolate Chip Cookie","",new Float(1.99)));
	this.pastries.add(new FoodItem(R.drawable.pfritter,"Fritter","",new Float(1.99)));
	
//    this.food.add(new FoodItem(R.drawable.coffee,"Cafe Latte","Tall, Soy, Decaf, Extra hot",new Float(3.29)));
//    this.food.add(new FoodItem(R.drawable.scone,"Blueberry scone","Fresh daily from Macarina Bakery",new Float(1.99)));    
    this.instant.add(new FoodItem(R.drawable.quickorder,"Instant order","",new Float(0.00f)));
    
}



}
