package angelhack.summer12;

public class FoodItem {
	public int picture;
	public String name,description;
	public Float price;
	public FoodItem(int pic,String n,String desc,Float pri)
	{
		this.picture= pic;
		this.name = n;
		this.description = desc;
		this.price = pri;
	}
	public String getname()
	{
		return name;
	}
	public String getdescription()
	{
		return description;
	}
	public String getprice()
	{
		return price.toString();
	}
	public int getpic()
	{
		return picture;
	}
}
